use nom::*;
use crate::types::*;

named!(parse_header_ids, tag!([0x1F, 0x8B]));
named!(parse_cm<CompressionMethod>, map!(take!(1), |b| CompressionMethod::from(b[0])));

// FIXME from_bits_truncate ignores any reserved flags which should be treated as error
named!(parse_flags<Flags>, map!(take!(1), |b| Flags::from_bits_truncate(b[0])));

// FIXME is there something simpler?
// FIXME is there already a type for posix time?
named!(parse_modification_time<u32>, do_parse!(ptime: le_u32 >> (ptime)));

// These flags are specific to the compression method. For the only known
// compression method these flags can be ignored.
named!(parse_extra_flags<u8>, do_parse!(xflags: le_u8 >> (xflags)));

