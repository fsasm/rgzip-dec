use bitflags::*;

#[derive(Debug, PartialEq)]
pub enum CompressionMethod {
    Deflate,
    Reserved,
    Invalid
}

impl From<u8> for CompressionMethod {
    fn from(b: u8) -> Self {
        match b {
            0...7 => CompressionMethod::Reserved,
            8 => CompressionMethod::Deflate,
            _ => CompressionMethod::Invalid
        }
    }
}

bitflags! {
    pub struct Flags : u8 {
        const TEXT    = 0b0000_0001;
        const HCRC    = 0b0000_0010;
        const EXTRA   = 0b0000_0100;
        const NAME    = 0b0000_1000;
        const COMMENT = 0b0001_0000;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compression_method() {
        for i in 0..7 {
            assert_eq!(CompressionMethod::from(i), CompressionMethod::Reserved);
        }
        assert_eq!(CompressionMethod::from(8), CompressionMethod::Deflate);
        for i in 9..255 {
            assert_eq!(CompressionMethod::from(i), CompressionMethod::Invalid);
        }
    }
}
